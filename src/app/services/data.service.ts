import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataItem } from '../interfaces/data-item';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<DataItem[]> {
    return this.http.get<DataItem[]>('http://localhost:4200/assets/jsons/data.json');
  }
}
