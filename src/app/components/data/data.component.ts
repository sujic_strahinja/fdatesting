import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ObservableLike, Observable } from 'rxjs';
import { DataItem } from 'src/app/interfaces/data-item';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {
  constructor(private dataService: DataService) { }

  private dataItems: any;
  private $dataItems: Observable<DataItem[]>;

  ngOnInit() {
    this.$dataItems = this.dataService.getAll();
    // this.dataService.getAll().subscribe(data => {
    //   this.dataItems = data;
    // });

    // console.log(this.dataService.getAll());
  }
}
